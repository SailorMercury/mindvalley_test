json.extract! linker, :id, :original_link, :hashing, :created_at, :updated_at
json.url linker_url(linker, format: :json)