class Linker < ActiveRecord::Base

  validates_presence_of :original_link
  before_save :generate_hash
  SALT = "any_long_random_string_will_do"
  
  
  def generate_hash
    self.hashing = Gibberish::MD5(SALT + self.original_link) if !self.original_link.blank?
  end

  def full_link
    if self.original_link[0..3] != "http"
      return "http://" + self.original_link
    else
      return self.original_link
    end
  end
  
end
