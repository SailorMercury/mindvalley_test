class TicTacToe

  WINNING_COMBINATIONS = [
    # Horizontal wins:
    [0, 1, 2], [3, 4, 5], [6, 7, 8],
    # Vertical wins:
    [0, 3, 6], [1, 4, 7], [2, 5, 8],
    # Diagonal wins:
    [0, 4, 8], [2, 4, 6]
  ]

  @@dimension = 3
  def self.start
    player = "X"
    cpu = "O"
    board = Array.new(@@dimension**2)
    puts "Starting game..."
    self.display_board(board)
    puts "do you want to start? (y/n)"
    start_first = gets
    start_first.chomp!
    while ['y','n'].exclude?(start_first)
      puts "invalid input, please choose (y/n)"
      start_first = gets
      start_first.chomp!
    end
    
    while board_winner(board)
    
      if start_first == 'y'
        self.player_move(board)
        start_first = nil
      end
      
      self.cpu_move(board)
      self.display_board(board)
      self.player_move(board)
    end
    self.display_board(board)
    
  end

  def self.display_board(array)
    string = ""
    array.each_with_index do |b, i|
      if b.nil?
        string << (" | " + i.to_s + " | ")
      else
        string << (" | " + b + " | ")
      end
      if (i+1)%@@dimension == 0
        string << "\n"
      end
    end
    puts string  
  end

  def self.valid_move?(array, move)
    if (0..8).include?(move) && array[move].nil?
      return true
    else
      puts "invalid move"
      return false
    end
  end
  
  def self.board_winner(arr)
    x = arr.each_index.select{|i| arr[i] == 'X'}
    o = arr.each_index.select{|i| arr[i] == 'O'}
    
    TicTacToe::WINNING_COMBINATIONS.each do |win|
      if (win - x).blank?
        puts "Winner is X"
        return false
      elsif (win - o).blank?
        puts "Winner is O" 
        return false
      end
    end 
    
    if arr.exclude?(nil)
      puts "DRAW"
      return false
    else
      return true
    end
  end
  
  def self.cpu_move(arr)
    #make cpu smart enough next time, now cpu will just randomly move
    available_move = arr.each_index.select{|i| arr[i] == nil}
    if !available_move.blank?
      arr[available_move.sample] = "O"
    end
  end
  
  def self.player_move(board)
    puts "Please enter 0 to 8" 
    move = gets
    move.chomp!
    while !self.valid_move?(board, move.to_i)
      move = gets
      move.chomp!
    end
    board[move.to_i] = "X"  
  end
end
