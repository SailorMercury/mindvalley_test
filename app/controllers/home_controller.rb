class HomeController < ApplicationController
  respond_to :json, only: [:resume_json]

	def index
	end
	
  def resume_json
    hash = {
      :resume => {
        :name => "Mercury Tang", 
        :age => "31", 
        :contact => "016-3177489", 
        :education => "Software Engineering Degree",
        :working_experience => "5 Years"},
      :cv => {
        :technical_skills => "Ruby on Rails, Java, mysql, mongodb, Hardwares assemblies",
        :personal_skills => "Teamwork, Communication, Eager to learn",
        :previous_experience => "Alpha Melon Sdn Bhd (3 years), Share Investor Pte Limited (1 year), MSP System Sdb Bhd (2 years)",
        :website_involved => "mytaman.com, bursamalaysia.com, becon.com.my, shareinvestor.com"}
    }
    render :json => JSON.pretty_generate(hash)#hash.to_json    
  end


end
