require 'test_helper'

class LinkersControllerTest < ActionController::TestCase
  setup do
    @linker = linkers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:linkers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create linker" do
    assert_difference('Linker.count') do
      post :create, linker: { hashing: @linker.hashing, original_link: @linker.original_link }
    end

    assert_redirected_to linker_path(assigns(:linker))
  end

  test "should show linker" do
    get :show, id: @linker
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @linker
    assert_response :success
  end

  test "should update linker" do
    patch :update, id: @linker, linker: { hashing: @linker.hashing, original_link: @linker.original_link }
    assert_redirected_to linker_path(assigns(:linker))
  end

  test "should destroy linker" do
    assert_difference('Linker.count', -1) do
      delete :destroy, id: @linker
    end

    assert_redirected_to linkers_path
  end
end
