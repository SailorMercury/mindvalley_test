class CreateLinkers < ActiveRecord::Migration
  def change
    create_table :linkers do |t|
      t.string :original_link
      t.string :hashing

      t.timestamps null: false
    end
  end
end
